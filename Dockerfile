FROM nginx:latest

LABEL maintainer="Rafael Oliveira"
LABEL version="0.1"
LABEL description="K8S-IMG-01"

COPY ./index.html /usr/share/nginx/index.html